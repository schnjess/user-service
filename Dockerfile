# user-service/Dockerfile
FROM golang:1.11.0 as builder

WORKDIR /go/src/gitlab.com/schnjess/user-service

COPY . .

RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo .


FROM debian:latest

RUN mkdir /app
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/schnjess/user-service/user-service .

CMD ["./user-service"]
