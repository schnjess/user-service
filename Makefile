build:
	protoc --plugin=protoc-gen-go=${GOPATH}/bin/protoc-gen-go \
	--plugin=protoc-gen-micro=${GOPATH}/bin/protoc-gen-micro \
	--proto_path=${GOPATH}/src:. --micro_out=. \
	--go_out=. proto/auth/auth.proto
	docker build -t user-service .

run:
	docker run --net="host" \
	-p 50053 \
	-e DB_HOST=localhost \
	-e DB_NAME=postgres \
	-e DB_PASSWORD=password \
	-e DB_USER=postgres \
	-e MICRO_SERVER_ADDRESS=:50051 \
	-e MICRO_REGISTRY=mdns \
	user-service
